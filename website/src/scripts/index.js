
import $ from 'jquery';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap';
import '../styles/index.scss';

let $container;

$(document).ready(function(){
    $container = $('#contentRow');
    
    /*$('#carouselWWD.carousel').carousel({
        interval: 1000
    });*/
    $('.accordion').click(function(){
        console.log('test');
        this.classList.toggle('Active');
        let panel = this.nextElementSibling;
        panel.style.display === 'block' ? panel.style.display = 'none' : panel.style.display = 'block';
    });
    $(".fancybox").fancybox({
		prevEffect	: 'none',
		nextEffect	: 'none',
		helpers	: {
			title	: {
				type: 'outside'
			},
			thumbs	: {
				width	: 50,
				height	: 50
			}
		}
	});
    /*
    $('.bxslider').bxSlider({
      mode: 'fade',
      captions: true,
      slideWidth: 600
    }); not a function? */
});
